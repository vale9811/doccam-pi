/**
 * An SSH Manager to control the SSH tunnels to the master server.
 * The manager communicates over an IPC socket with the name 'sshManager'.
 */

const ipc = require('node-ipc');
const autossh = require('autossh');
const { exec } = require("child_process");
const Fs = require('fs')

///////////////////////////////////////////////////////////////////////////////
//                                Declarations                               //
///////////////////////////////////////////////////////////////////////////////

/**
 * SSH Tunnels in the format: [host:port] : [autossh instance].
 */
const tunnels = {};

// IPC Setup
ipc.config.id = 'sshMan';
ipc.config.retry = 1500;
ipc.config.silent = true;

ipc.serve();
const server = ipc.server;

///////////////////////////////////////////////////////////////////////////////
//                                    Code                                   //
///////////////////////////////////////////////////////////////////////////////

// Start the IPC Server
server.start();

console.log('SSH-Manager is up and running.');
console.log(`The Server is listening on socket: "${ipc.server.path}"`);

function setPermission() {
    if (!Fs.existsSync(ipc.server.path)) {
            setTimeout(setPermission, 1000);
            return;
    }
    
    exec("chmod g+rwx " + ipc.server.path);
}

setPermission();

function getTunnelIdentifier(host, port) {
    return host + ":" + port;
}

/**
 * The request handler to create a tunnel. Replies with an error and the remote port of the tunnel.
 */
server.on('create_tunnel', (data, socket) => {
    let id = data.id;
    let replySuccess = generateReplyFunction('success', socket, id);
    let replyError = generateReplyFunction('error', socket, id);
    let tunnel;

    tunnel = tunnels[getTunnelIdentifier(data.localHost, data.localPort)];

    // If the tunnel already exists:
    if (tunnel && (tunnel.info.localHost === 'localhost' || tunnel.info.localHost === data.localHost)) {
        return replySuccess(tunnel.info.remotePort);
    }

    // Dummy for other request
    tunnels[getTunnelIdentifier(data.localHost, data.localPort)] = {
        info: {
            localHost: data.localHost || 'localhost'
        }
    };

    // Let's create a tunnel!
    return createNewTunnel(data).then((tunnel) => {
        tunnels[getTunnelIdentifier(tunnel.info.localHost, tunnel.info.localPort)] = tunnel;
        replySuccess(tunnel.info.remotePort);
	console.log("Created Tunnel:\n", JSON.stringify(tunnel.info));
    }, (error) => {
        console.error("Tunnel Creation Failed:\n", error);
        replyError(error);
        delete tunnels[getTunnelIdentifier(data.localHost, data.localPort)];
    });
});

/**
 * The request handler to close a tunnel. The reply is {error: [message]} in case of an error, otherwise {success: true}.
 */
server.on('close_tunnel', ({
    port,
    id,
    ip
}, socket) => {
    let replySuccess = generateReplyFunction('success', socket, id);
    let replyError = generateReplyFunction('error', socket, id);
    let tunnel;

    tunnel = tunnels[getTunnelIdentifier(ip, port)];

    if (!tunnel)
        return replySuccess();

    // Kill the tunnel and clean up.
    try {
        tunnel.kill();
    } catch(e) {
        return replyError("Error closing the tunnels.");
    }

    console.log("Tunnel Closed:\n", tunnel);

    delete tunnels[getTunnelIdentifier(ip, port)];
    return replySuccess();
});

/**
 * Utility
 */

function createNewTunnel(options) {
    return new Promise((resolve, reject) => {
        try {
            let tunnel = autossh(options);
	    let timeout = setTimeout(() => {
		tunnel.kill();
		reject('Connection Timeout');
	    }, options.connectTimeout);
	    
            // TODO: BETTER ERROR HANDLING
            tunnel.on('connect', connection => {
                // Wait for Exit // TODO: Nicer
		clearTimeout(timeout);
                setTimeout(() => resolve(tunnel), 1000);
            });

            tunnel.on('error', (error, killed) => {
                // If auto SSH or SSH itself issue an error. // TODO: Investigate
                if (killed && (error.message && error.message.indexOf("refused") > -1)) {
                    tunnel.kill();
                    reject(error.message || error);
                }

                return;
            });
        } catch (error) {
            reject(error);
        }
    });
}

function generateReplyFunction(message, socket, id) {
    return (data) => server.emit(socket, message + id, data);
}
