/** Outsourced definition of the actions and simple action creators for trivial actions.
 * @module Actions
 * @todo allowed values
 */

const Promise = require('promise');
const writeConfig = require('./utility/config.js').write;
const readConfig = require('./utility/config.js').read;
const fixupTypes = require('./utility/config.js').fixupTypes;

///////////////////////////////////////////////////////////////////////////////
//                                Declarations                               //
///////////////////////////////////////////////////////////////////////////////

/**
 * Actions
 * The action definitions.
 */
let actions = {
    UPDATE_CONFIG: 'UPDATE_CONFIG',
    REQUEST_START: 'REQUEST_START',
    SET_STARTED: 'SET_STARTED',
    REQUEST_STOP: 'REQUEST_STOP',
    SET_STOPPED: 'SET_STOPPED',
    REQUEST_RESTART: 'REQUEST_RESTART',
    TAKE_SNAPSHOT: 'TAKE_SNAPSHOT',
    SET_SNAPSHOT_FAILED: 'SET_SNAPSHOT_FAILED',
    SET_SNAPSHOT_TAKEN: 'SET_SNAPSHOT_TAKEN',
    SET_ERROR: 'SET_ERROR',
    SET_ERROR_RESOLVED: 'SET_ERROR_RESOLVED',
    TRY_RECONNECT: 'TRY_RECONNECT',
    STOP_ERROR_HANDLING: 'STOP_ERROR_HANDLING',

    // Communicator
    SET_CONNECTED: 'SET_CONNECTED',
    SET_DISCONNECTED: 'SET_DISCONNECTED',

    // SSH
    SSH_CREATE_TUNNEL: 'SSH_CREATE_TUNNEL',
    SET_SSH_DISCONNECTED: 'SET_SSH_DISCONNECTED',
    SET_SSH_DISCONNECTING: 'SET_SSH_DISCONNECTING',
    SET_SSH_CONNECTING: 'SET_SSH_CONNECTING',
    SET_SSH_PORT: 'SET_SSH_PORT',
    SET_SSH_ERROR: 'SET_SSH_ERROR',
    SET_SSH_WILL_RECONNECT: 'SET_SSH_WILL_RECONNECT',
    SET_SSH_CONNECTED: 'SET_SSH_CONNECTED',
    SET_SSH_MAN_CONNECTED: 'SET_SSH_MAN_CONNECTED',
    SET_SSH_MAN_DISCONNECTED: 'SET_SSH_MAN_DISCONNECTED',
    
    // Master Server Related
    HYDRATE: 'HYDRATE'
};

/**
 * Trivial Action Creators
 */
let creators = {};

///////////////////////////////////////////////////////////////////////////////
//                                    Code                                   //
///////////////////////////////////////////////////////////////////////////////

/**
 * Updates or initializes the configuration state. Updating and initializing are sync actions. The disk write happens asynchronously. Also sets the name if it is mentioned in the update.
 * @param { Object } update The new (partial) config. If it is undefined, then the configuration will be read from disk.
 * @returns { Promise } A thunk for redux-thunk.
 * @throws { Error } In case the Config can't be read.
 */
creators.updateConfig = function(update) {
    return (dispatch, getState) => {
        let init = false;

        if (!update) {
            let { modified, config, error } = readConfig();
	    init = !modified;
	    update = config;
            // TODO: Proper handling.
            if (error)
                throw new Error(error);
        }

	try {
	     update = fixupTypes(update);
	} catch(e) {
	    return Promise.reject(e);
	}
	
	let newConfig = Object.assign({}, getState().config, update);

        // TODO: CD
	return new Promise((resolve, reject) =>{
	    (() => {
		if (init) {
		    return Promise.resolve('Config Updated.');
		}
		else
		    return writeConfig(newConfig);
	    })().then(() => {
		dispatch({
		    type: actions.UPDATE_CONFIG,
		    data: update
		});

		resolve('Config updated.');
	    }).catch(() => reject());
	});
    };
};

creators.requestStart = function() {
    return {
        type: actions.REQUEST_START
    };
};

creators.requestStop = function() {
    return {
        type: actions.REQUEST_STOP
    };
};

creators.setStarted = function() {
    return {
        type: actions.SET_STARTED
    };
};

creators.setStopped = function() {
    return {
        type: actions.SET_STOPPED
    };
};

creators.requestRestart = function() {
    return {
        type: actions.REQUEST_RESTART
    };
};

creators.takeSnapshot = function() {
    return {
        type: actions.TAKE_SNAPSHOT
    };
};

creators.setSnapshotFailed = function(error) {
    return {
        type: actions.SET_SNAPSHOT_FAILED,
        data: error
    };
};

creators.setSnapshotTaken = function() {
    return {
        type: actions.SET_SNAPSHOT_TAKEN
    };
};

// Optional error Message for logging.
creators.setError = function(error, stdout, stderr) {
    return {
        type: actions.SET_ERROR,
        data: error,
        stdout,
        stderr
    };
};

creators.setErrorResolved = function() {
    return {
        type: actions.SET_ERROR_RESOLVED
    };
};

creators.setTryReconnect = function(to) {
    return {
        type: actions.TRY_RECONNECT,
        to
    };
};

creators.stopErrorHandling = function() {
    return {
        type: actions.STOP_ERROR_HANDLING
    };
};

creators.setConnected = function() {
    return {
        type: actions.SET_CONNECTED
    };
};

creators.setDisconnected = function() {
    return {
        type: actions.SET_DISCONNECTED
    };
};

creators.setSSHPort = function(port, tunnel) {
    return {
	type: actions.SET_SSH_PORT,
        data: { port, tunnel }
    };
};

creators.setSSHConnected = function(tunnel) {
    return {
        type: actions.SET_SSH_CONNECTED,
	data: { tunnel }
    };
};

creators.setSSHConnecting = function(tunnel) {
    return {
        type: actions.SET_SSH_CONNECTING,
	data: { tunnel }
    };
};

creators.setSSHDisconnecting = function(tunnel) {
    return {
        type: actions.SET_SSH_DISCONNECTING,
	data: { tunnel }
    };
};

creators.setSSHDisconnected = function(tunnel) {
    return {
        type: actions.SET_SSH_DISCONNECTED,
	data: { tunnel }
    };
};

creators.setSSHError = function(error, tunnel) {
    return {
        type: actions.SET_SSH_ERROR,
        data: { error, tunnel }
    };
};

creators.setSSHWillReconnect = function(tunnel) {
    return {
        type: actions.SET_SSH_WILL_RECONNECT,
	data: { tunnel }
    };
};

creators.setSSHManConnected = function() {
    return {
	type: actions.SET_SSH_MAN_CONNECTED
    };
};

creators.setSSHManDisconnected = function() {
    return {
	type: actions.SET_SSH_MAN_DISCONNECTED
    };
};

/**
 * Create a tunnel Object in state.
 * @param { !String } nam  A unique ID.
 * @param { !function[String] } getIP A function that returns the target ip address of the server.
 * @param { !function[Numbber] } getPort A function that returns the target ip address of the server.
 * @param { !function[bool] } getEnabled A function wether the tunnel is enabled.
 */
creators.sshCreateTunnel = function(name, getIP, getPort, getEnabled) {
    return {
	type: actions.SSH_CREATE_TUNNEL,
	data: { name, getIP, getPort, getEnabled }
    };
};

module.exports = {
    actions,
    creators
};
