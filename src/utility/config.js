/** Config read/write utilities.
 * @module Utilities/Config
 */

const Promise = require('promise');
const fs = require('fs');
const logger = require('../logger.js');

// TODO: CD

module.exports = {};

// TODO Default CFG
/**
 * Read the JSON config file.
 * @returns { Object | bool } Returns the parsed config or false in case of anb error.
 */
module.exports.read = function() {
    try {
	let conf = JSON.parse(fs.readFileSync('./config.js').toString()); //TODO: Nicer
	let problems = module.exports.check(conf);

	let error = '';
	for(let problem of problems)
	    if(problem[2] == module.exports.severity.SEVERE) {
		error += `Severe problems with config field ${problem[0]}: ${problem[1]}\n`;
	    }

	if(error)
	    return { error };

	return Object.assign(module.exports.fillWithDefaults(conf),
			     { error: false });
    } catch (e) {
	return { error: e };
    }
};

/**
 * Writes the config to disk.
 * @param { Object } config The configuration to write.
 * @returns { Promise } A promise for writing the configuration. (returns errors upon rejection)
 */
module.exports.write = function(config) {
    return new Promise((resolve, reject) => {
	let problems = module.exports.check(config);

	if(problems.length)
	    reject({type: 'config_input_error', details: problems});
	
	fs.writeFile('./config.js',
		     JSON.stringify(Object.assign(config, {
			 unconfigured: false
		     }), undefined, 2),
		     err => {
			 if (err)
			     reject({type: 'misc_error', details: err.message}); // TODO: Log // TODO: bCD
			 else
			     resolve('Config has been Written.'); // TODO: CD
		     });
    });
};

module.exports.severity = {
    NORMAL: 0,
    SEVERE: 1
};

/**
 * Checks the config.
 * @param { Object } config The configuration to check.
 * @returns { Array } A list of problems in the format: [<key>, <problem desc>]
 */
module.exports.check = function(config) {
    let sev = module.exports.severity;
    let problems = [];
    let checkIfEmpty = (list, severity) => {
	for(let item of list)
	    if(!config[item])
		problems.push([item, "cannot be empty", severity]); // TODO: CD
    };

    const notEmptySevere = ['master'];
    const notEmpty = ['master'];
    const notEmptySsh = ['sshUser','sshLocalPort', 'sshLocalUser', 'sshPort', 'name'];

    checkIfEmpty(notEmptySevere, sev.SEVERE);
    
    if(config.unconfigured)
	return problems;

    checkIfEmpty(notEmpty, sev.NORMAL);
    if(config.ssh)
	checkIfEmpty(notEmptySsh, sev.NORMAL);

    return problems;
};

module.exports.configDefaults = {
    "camIP": ['string', ''],
    "camPort": ['number', 0],
    "rtspUser": ['string', ''],
    "rtspPassword": ['string', ''],
    "camPanelPort": ['number', 0],
    "camProfile": ['string', ''],
    "snapProfile": ['string', ''],
    "stream-server": ['string', ''],
    "key": ['string', ''],
    "master": ['string', ''],
    "authMaster": ['string', ''],
    "sshMaster": ['string', () => this.master],
    "ffmpegPath": ['string', 'ffmpeg'],
    "ssh": ['boolean', false],
    "sshUser": ['string', ''],
    "sshLocalUser": ['string', ''],
    "sshConnectTimeout": ['number', 5000],
    "sshPort": ['number', 0],
    "sshLocalPort": ['number', 22],
    "camSSHEnabled": ['boolean', false],
    "piSSHEnabled": ['boolean', false],
    "customOutputOptions": ['string', ''],
    "customInputOptions": ['string', ''],
    "customVideoOptions": ['string', ''],
    "customAudioOptions": ['string', ''],
    "sshKey": ['string', ''],
    "name": ['string', `${Math.random() * 1000}`],
    "camPanelAuth": ['string', ''],
    "unconfigured": ['boolean', true]
};
Object.freeze(module.exports.configTypeMap);

/**
 * Interpolate config with default values.
 * Strip config values that are not defined in the spec above.
 * @param { Object } config
 * @returns { { bool, Object } } Wether the config has been modified an the config itself.
 */
module.exports.fillWithDefaults = function (config) {
    const defaults = module.exports.configDefaults;
    let modified = false;

    for(let key in config) {
	if(!(key in module.exports.configDefaults)) {
	    delete config[key];
	    continue;
	}
    }
    
    for(let key in defaults) {
	if(!(key in config)) {
	    config[key] = defaults[key][1];
	    modified = true;
	}
    }

    return { modified, config };
};


/**
 * Fix up weirded up types from the web interface.
 * @param { Object } config
 * @returns { Object } fixed configg
 * @throws { Error } if a fix can't be applied
 */
module.exports.fixupTypes = function(config) {
    for(let key of Object.keys(config)) {
	if(typeof config[key] === module.exports.configDefaults[key][0])
	    continue;

	switch (module.exports.configDefaults[key][0]) {
	case 'number':
	    config[key] = parseInt(config[key]);
	    if(isNaN(config[key]))
		throw new Error('Invalid config for: ' + key);
	    break;
	case 'boolean':
	    config[key] = config[key] === 'true';
	    break;
	default:
	    break;
	}
    }
    
    return config;
};
