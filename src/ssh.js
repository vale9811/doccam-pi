/**
 * @module ssh
 * @description Manages the SSH Tunnels to the master-server. (Even works with muliple instances.)
 * @description The SSH Manager is commanded by the Communicator.
 */

// TODO: Update Webui
// TODO: Disconnect event from SSH-Manager itself!

const ipc = require("@node-ipc/node-ipc").default;
const {
    sshCreateTunnel,
    setSSHError,
    setSSHConnecting,
    setSSHConnected,
    setSSHDisconnecting,
    setSSHDisconnected,
    setSSHWillReconnect,
    setSSHPort,
    setSSHManConnected,
    setSSHManDisconnected,
} = require("./actions").creators;

const { UPDATE_CONFIG } = require("./actions").actions;

///////////////////////////////////////////////////////////////////////////////
//                                Declarations                               //
///////////////////////////////////////////////////////////////////////////////

// Object oriented `this`.
let self;

// Get state for the SSH State.
let getState, getConfig;

// The redux dispatch function to alter the state.
let dispatch;

// PIDs of the ssh-tunnel and the camera tunnel.
let sshPID, camPID;

// IPC Setup
ipc.config.silent = true;
ipc.config.retry = 1500;
ipc.config.maxRetries = Infinity;
ipc.config.stopRetry = false;

/**
 * @function getPorts
 * A function to get the Ports for the SSH-Tunnel. Given as parameter for the @see SSHMan.
 * This module doesn't care where it comes from, as long as it WORKS!
 * @returns {Promise} Resolves to ports {camForwardPort: integer, sshForwardport: integer}.
 */
let getPorts;

///////////////////////////////////////////////////////////////////////////////
//                                    Code                                   //
///////////////////////////////////////////////////////////////////////////////

// TODO: Check if autossh installed
/**
 * A Class to abstract the communication with the SSH-Manager.
 * @param {} _getState
 * @param {} _getConfig
 * @param {} _dispatch
 * @param {} _getPorts @see `let getPorts`
 * @throws {}
 */
class SSHMan {
    constructor(_getState, _getConfig, _dispatch, _getPorts) {
        // Signleton
        if (self) return;

        if (typeof _getState !== "function") {
            throw new Error("Invalid getState() function.");
        }

        if (typeof _getConfig !== "function" || !_getConfig()) {
            throw new Error("Please load a valid config.");
        }

        if (typeof _dispatch !== "function") {
            throw new Error("Invalid dispatch function.");
        }

        if (
            typeof _getPorts !== "function" &&
            typeof _getConfig() !== "object"
        ) {
            throw new Error("Invalid getPorts function.");
        }

        getState = _getState;
        getConfig = _getConfig;
        getPorts = _getPorts;
        dispatch = _dispatch;

        // Create Tunnels - TODO: Central management...
        // TODO: Add Error checking.... is in getEnabled for now...
        dispatch(
            sshCreateTunnel(
                "pi",
                () => "localhost",
                () => getConfig().sshLocalPort,
                () => getConfig().piSSHEnabled,
            ),
        );
        dispatch(
            sshCreateTunnel(
                "cam",
                () => getConfig().camIP,
                () => getConfig().camPanelPort,
                () =>
                    getConfig().camSSHEnabled &&
                    getConfig().camPanelPort &&
                    getConfig().camIP,
            ),
        );
        self = this;

        connectIpc();
    }
}

/**
 * Action Creators.
 */

// TODO: disconnect on disable
/**
 * Action Creator to connect a SSH Tunnel via the SSH-Manager.
 * @param tunnel - A tunnel object.
 * @returns { Array[Promise] }
 */
SSHMan.prototype.connect = function (tunnel) {
    return (dispatch, getState) => {
        tunnel = getTunnelStatus(tunnel);
        if (!getConfig().ssh) return Promise.reject("SSH is disabled."); // TODO: CD

        if (!tunnel.getEnabled()) return Promise.reject("Tunnel disabled.");

        return new Promise((resolve, reject) => {
            let name = tunnel.name;
            if (tunnel.status == "CONNECTED") {
                resolve(tunnel);
            }

            if (tunnel.status !== "DISCONNECTED") {
                reject("Conflict with other action.");
            }

            // Let's go ahead.
            dispatch(setSSHConnecting(name));

            isIpcConnected()
                .catch(() => {
                    dispatch(
                        setSSHError("No connection to SSH Manager.", name),
                    );
                    dispatch(setSSHWillReconnect(name));
                    reject({ details: "No connection to SSH Manager", tunnel });
                })
                .then(() => {
                    createTunnel(tunnel.getPort(), tunnel.getIP())
                        .then((port) => {
                            dispatch(setSSHPort(port, name));
                            resolve(tunnel);
                        })
                        .catch((error) => {
                            dispatch(setSSHError(error, name));
                            reject({ tunnel, details: error });
                        });
                });
        });
    };
};

/**
 * Action Creator to connect the SSH Tunnels via the SSH-Manager.
 * @returns { Array[Promise] }
 */
SSHMan.prototype.connectAll = function () {
    return (dispatch, getState) => {
        let config = getState().config;
        if (!config.ssh) return [Promise.reject("SSH is disabled.")]; // TODO: CD

        let promises = [];
        for (let tunnel of getTunnels()) {
            promises.push(dispatch(self.connect(tunnel)));
        }
        return promises;
    };
};

/**
 * An action creator for disconnecting one SSH Tunnel.
 * @param tunnel - A tunnel object.
 * @returns { Promise }
 */
SSHMan.prototype.disconnect = function (tunnel) {
    return (dispatch, getState) => {
        tunnel = getTunnelStatus(tunnel);
        if (!getConfig().ssh) return Promise.reject("SSH is disabled."); // TODO: CD

        return new Promise((resolve, reject) => {
            let name = tunnel.name;
            if (tunnel.status !== "CONNECTED") {
                reject("Tunnel not connected.");
            }

            // Let's go ahead.
            dispatch(setSSHDisconnecting(name));

            isIpcConnected()
                .catch(() => {
                    dispatch(
                        setSSHError("No connection to SSH Manager.", name),
                    );
                    dispatch(setSSHWillReconnect(name));
                    reject({ details: "No connection to SSH Manager", tunnel });
                })
                .then(() => {
                    closeTunnel(tunnel.getIP(), tunnel.getPort())
                        .then(() => {
                            dispatch(setSSHDisconnected(tunnel.name));
                            resolve(tunnel);
                        })
                        .catch((error) => {
                            dispatch(setSSHError(error, name));
                            reject({ tunnel, details: error });
                        });
                });
        });
    };
};

/**
 * Action Creator to connect the SSH Tunnels via the SSH-Manager.
 * @returns { Array[Promise] }
 */
SSHMan.prototype.connectAll = function () {
    return (dispatch, getState) => {
        let config = getState().config;
        if (!config.ssh) return [Promise.reject("SSH is disabled.")]; // TODO: CD

        let promises = [];
        for (let tunnel of getTunnels()) {
            promises.push(dispatch(self.connect(tunnel)));
        }
        return promises;
    };
};

/**
 * An action creator for disconnecting the SSH Tunnels.  Resolves on
 * successfull disconnect. Rejects if the request couldn't be made. In
 * both cases the tunnels are disconnected.  It also sets the
 * willReconnect flag to false.
 * @returns { Array[Promise] }
 */
SSHMan.prototype.disconnectAll = function () {
    return (dispatch, getState) => {
        let promises = [];
        for (let tunnel of getTunnels()) {
            promises.push(dispatch(self.disconnect(tunnel)));
        }

        return promises;
    };
};

/**
 * An action creator for reconnecting a tunnel.
 * Resolves on successfull reconnect. Reject if either the disconnection or conncetion processes fail.
 * @returns { Promise }
 */
SSHMan.prototype.reconnect = function (tunnel) {
    return (dispatch, getState) => {
        let name = tunnel.name;
        return dispatch(self.disconnect(tunnel))
            .catch(() => Promise.resolve())
            .finally(() => dispatch(self.connect(tunnel)));
    };
};

/**
 * An action creator that restarts the SSH Tunnels.
 * Ignores deactivated tunnels.
 * @returns { Promise }
 */
SSHMan.prototype.reconnectAll = function () {
    return (dispatch, getState) => {
        let promises = [];
        for (let tunnel of getTunnels()) {
            if (tunnel.getEnabled())
                promises.push(dispatch(self.reconnect(tunnel)));
        }
        return promises;
    };
};

module.exports = SSHMan;

/**
 * Private Utility
 */

function connectIpc(ports) {
    ipc.connectTo("sshMan"); // TODO: CD

    // Register Events
    ipc.of.sshMan.on("connect", ipcConnected);
    ipc.of.sshMan.on("disconnect", ipcDisconnected);
}

/**
 * Handler for the IPC Connection Event.
 * Currently just sets connected = true.
 */
function ipcConnected() {
    dispatch(setSSHManConnected());
    for (let tunnel of getTunnels()) {
        if (tunnel.willReconnect) {
            dispatch(self.connect(tunnel)).catch(() => {
                // We do nothing...
            });
        }
    }
}

/**
 * Handler for the IPC Disconnect Event.
 */
function ipcDisconnected() {
    if (getState().ssh.status === "DISCONNECTED") return;

    // Automatically attempt to reconnect once connection is made.
    for (let tunnel of getTunnels()) dispatch(setSSHWillReconnect(tunnel.name));

    dispatch(setSSHManDisconnected());
}

// TODO/URGENT: BETTER ERROR HANDLING

/**
 * Creates a tunnel by reqesting it from the manager.
 * @param { number } localPort
 * @param { number } remotePort
 * @returns { function } Action / Promise
 */
function createTunnel(localPort, host) {
    return new Promise((resolve, reject) => {
        let id =
            new Date().getTime() + Math.random() * 100 + Math.random() * 200;
        let config = getConfig();

        // a sane minimum // FIXME: log a warning
        config.sshConnectTimeout =
            config.sshConnectTimeout < 1000 ? 1000 : config.sshConnectTimeout;

        let connectTimeout = setTimeout(
            () => reject("IPC Timeout. Can't reach SSH-Manager."),
            config.sshConnectTimeout + 1000,
        ); // TODO: CD

        // TODO: Variable CD
        ipc.of.sshMan.once("success" + id, (port) => {
            resolve(port); // TODO: That's ok for now, but should be auto-determined by the SSH-Manager further down the road...
        });

        ipc.of.sshMan.once("error" + id, (error) => {
            reject(error);
        });

        // In case of an Error in the IPC Connection.
        ipc.of.sshMan.once("error", (error) => {
            clearTimeout(connectTimeout);
            reject(error);
        });

        ipc.of.sshMan.emit("create_tunnel", {
            id: id,
            host: config.sshMaster,
            username: config.sshUser,
            sshPort: config.sshPort,
            localPort: localPort,
            remotePort: "auto",
            localHost: host,
            sshKey: config.sshKey,
            serverAliveInterval: 30,
            useAutossh: true,
            reverse: true,
            connectTimeout: config.sshConnectTimeout,
        });
    });
}

/**
 * Closes the SSH-Tunnel on the port.
 * @param {number} port
 * @returns {fuction} Action / Promise
 */
function closeTunnel(ip, port) {
    return new Promise((resolve, reject) => {
        let id = new Date().getTime();

        let connectTimeout = setTimeout(
            () => reject("IPC Timeout. Can't reach SSH-Manager."),
            getConfig().sshConnectTimeout,
        ); // TODO: CD

        ipc.of.sshMan.on("success" + id, (port) => {
            clearTimeout(connectTimeout);
            resolve(); // TODO: That's ok for now, but should be auto-determined by the SSH-Manager further down the road...
        });

        ipc.of.sshMan.on("error" + id, (error) => {
            reject();
        });

        // In case of an Error in the IPC Connection.
        ipc.of.sshMan.on("error", (error) => {
            clearTimeout(connectTimeout);
            reject();
        });

        ipc.of.sshMan.emit("close_tunnel", {
            id: id,
            port,
            ip,
        });
    });
}

/**
 * Helper to tell if the IPC connection works.
 * @returns {Promise}
 */
function isIpcConnected() {
    if (getState().ssh.status === "CONNECTED") return Promise.resolve();
    else return Promise.reject();
    //     // Wait a bit and try again.
    //     return new Promise((resolve, reject) => {
    //         ipc.of.sshMan.once('connect', () => resolve());
    //         ipc.of.sshMan.once('error', () => {
    // 	    for(let tunnel of getTunnels())
    // 		dispatch(setSSHWillReconnect(tunnel));
    //             reject('Cannot connect to the SSH-Manager.');
    //         });
    //     });
}

/**
 * Helper for creating an iterable list of getTunnels().
 * @returns { Array } An array of tunnels.
 */
function getTunnels() {
    let tmp = [];
    const tunnels = getState().ssh.tunnels;
    Object.keys(getState().ssh.tunnels).map((key) => {
        tmp.push(tunnels[key]);
    });

    return tmp;
}

/**
 * Get the up-to-date status of a tunnel.
 * @param { Object } tunnel
 * @returns { Object } tunnel
 */
function getTunnelStatus(tunnel) {
    return getState().ssh.tunnels[tunnel.name];
}

/**
 * Redux Middleware
 * Try to reconnect upon config change.
 */
SSHMan.middleware = (store) => (next) => (action) => {
    let result = next(action); // Let it pass...

    if (!self) return true;

    // If sth. has changed, we restart.
    if (action.type === UPDATE_CONFIG) {
        if (
            action.data.ssh ||
            action.data.sshPort ||
            action.data.sshMaster ||
            action.data.camIP ||
            action.data.camPanelPort
        ) {
            Promise.all(dispatch(self.reconnectAll())).catch(() => {}); // TODO: Nicer error handling
        }

        for (let tunnel of getTunnels()) {
            if (tunnel.name + "SSHEnabled" in action.data) {
                if (action.data[tunnel.name + "SSHEnabled"])
                    // FIXME: webclient rework
                    dispatch(self.reconnect(tunnel)).catch(() => {});
                else dispatch(self.disconnect(tunnel)).catch(() => {});
            }
        }
    }
    return result;
};
